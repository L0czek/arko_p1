
.data

.eqv SYS_READLINE 8
.eqv SYS_WRITELINE 4
.eqv SYS_OPEN 13
.eqv SYS_READ 14
.eqv SYS_WRITE 15
.eqv SYS_READINT 5
.eqv SYS_CLOSE 16
.eqv SYS_WRITE 15
.eqv SYS_PRINT_INT 1
.eqv SYS_EXIT 17
np: .asciiz "Podaj nazwe pliku >"
.align 2
error: .asciiz "Blad otwarcia pliku.\n"
.align 2
menu: .asciiz "Wybierz operacje.\n\t1 => Zmiana kontrastu\n\t2 => Rozciaganie histogramu\n\t3 => Wyjście\nopcja> "
.align 2
ferror: .asciiz "To nie jest plik bmp 24 bitowy\n"
.align 2
pkontrast: .asciiz "Podaj współczynnik zmiany kontrastu ( 100 to kontrast bazowy) \nkontrast> "
.align 2
wynik: .asciiz "Podaj nazwe pliku wynikowego> "
.align 2
error_w: .asciiz "Nie mozna otworzyć pliku do zapisu\n"
.align 2
fileSize: .word 0
path: .space 4096
.byte '\n'
.align 2
LUT: .space 256
.align 2
LUT2: .space 256
.align 2
LUT3: .space 256
.align 2
bitmapFileHeader: .space 14
.align 2 
bitmap: .space 4294967296  # 0xa000000

.text
main:
	li $v0, SYS_WRITELINE # ask for input file name
	la $a0, np
	syscall
	
	li $v0, SYS_READLINE # read input file name
	la $a0, path
	li $a1, 4096
	syscall
	
	la $t0, path
	
loop1:
	addiu $t0, $t0, 1
	lbu $t1, -1($t0)
	bne $t1, '\n', loop1
	sb $zero, -1($t0)
	
	li $v0, SYS_OPEN # try to open file
	la $a0, path
	li $a1, 0
	li $a2, 0
	syscall
	
	bne $v0, -1, chose_opt
	
	li $v0, SYS_WRITELINE # print error message
	la $a0, error
	syscall
	j main
	
chose_opt:
	move $s0, $v0
	li $v0, SYS_WRITELINE
	la $a0, menu
	syscall
	
	li $v0, SYS_READINT
	syscall
	sgt $t0, $v0, 3
	sle $t1, $v0, 0
	or $t0, $t0, $t1
	beq $t0, 1, chose_opt
	beq $v0, 3, pexit
	move $s1, $v0
	
	li $v0, SYS_READ
	move $a0, $s0
	la $a1, bitmapFileHeader+2
	li $a2, 14
	syscall
	
	lw $t0, bitmapFileHeader
	beq $t0, 0x4d420000, read_bmp
	li $v0, SYS_WRITELINE
	la $a0, ferror
	syscall
	j main
		
	
read_bmp:
	lw $t0, bitmapFileHeader+4
	sw $t0, fileSize
	
	li $v0, SYS_READ
	move $a0, $s0
	la $a1, bitmap
	move $a2, $t0
	syscall
	
	li $v0, SYS_CLOSE
	move $a0, $s0
	syscall	
	
	beq $s1, 2, histogram
		
read_contrast:
	li $v0, SYS_WRITELINE
	la $a0, pkontrast
	syscall
	
	li $v0, SYS_READINT
	syscall
	
	move $t0, $v0
	slt $t1, $t0, $zero
	bnez $t1, read_contrast
	
	li $t1, 0
	la $t2, LUT
	li $t6, 0xff
make_lut_contrast:	
	
	subiu $t4, $t1, 127
	mul $t4, $t4, $t0
	div $t4, $t4, 100
	addiu $t4, $t4, 127
	
	sgt $t5, $t4, 255
	beq $t5, 1, write_0xff
	
	slt $t5, $t4, $zero
	beq $t5, 1, write_0x00
	
	
write_value:	
	sb $t4, LUT($t1)
	j LUT_contrast_end
write_0xff:
	sb $t6, LUT($t1)
	j LUT_contrast_end
write_0x00:
	sb $zero, LUT($t1)
LUT_contrast_end:
	addiu $t1, $t1, 1
	bne $t1, 256, make_lut_contrast

	
rewrite_image:
#break
	lw $t1, bitmapFileHeader+12   # offset to pixel array
	lw $t2, bitmapFileHeader+0x14 # width
	lw $t3, bitmapFileHeader+0x18 # height
	mul $s3, $t2, 3	# number of subpixels
	
	mul $t0, $t2, 3
	addiu $t0, $t0, 3
	div $t0, $t0, 4
	mul $t0, $t0, 4
	
	
	move $s2, $t0 # row size
	la $t0, bitmapFileHeader+2
	add $t0, $t1, $t0
	move $t4, $zero # i
outer_loop:
	move $t5, $zero # j
inner_loop:
	add $t6, $t0, $t5
	lbu $t7, 0($t6)
	lbu $t7, LUT($t7)
	sb $t7, 0($t6)
	addiu $t5, $t5, 1
	bne $t5, $s3, inner_loop
	
	add $t0, $t0, $s2
	addiu $t4, $t4, 1
	bne $t4, $t3, outer_loop
	
	
write_result:
	li $v0, SYS_WRITELINE
	la $a0, wynik
	syscall
	
	li $v0, SYS_READLINE
	la $a0, path
	li $a1, 4096
	syscall
	
	la $t0, path
	
loop2:
	addiu $t0, $t0, 1
	lbu $t1, -1($t0)
	bne $t1, '\n', loop2
	sb $zero, -1($t0)
	
	li $v0, SYS_OPEN # try to open file
	la $a0, path
	li $a1, 1
	li $a2, 0
	syscall
	
	bne $v0, -1, write
	li $v0, SYS_WRITELINE
	la $a0, error_w
	syscall
	j write_result
		
write:	
	
	move $s4, $v0
	move $a0, $s4
	li $v0, SYS_WRITE
	la $a1, bitmapFileHeader+2
	lw $a2, fileSize
	syscall
	
	li $v0, SYS_CLOSE
	move $a0,$s4
	syscall
	
	j main
pexit:
	li $v0, SYS_EXIT
	move $a0, $zero
	syscall
	
histogram:	
	subiu $sp, $sp, 8
	lw $t1, bitmapFileHeader+12   # offset to pixel array
	lw $t2, bitmapFileHeader+0x14 # width
	lw $t3, bitmapFileHeader+0x18 # height
	mul $t0, $t2, 3
	addiu $t0, $t0, 3
	div $t0, $t0, 4
	mul $s0, $t0, 4	# row size in bytes
	sw $s0, 0($sp)
	
	la $t0, bitmapFileHeader+2
	add $t0, $t0, $t1
	mul $t1, $t2, 3 # row size in pixels
	sw $t1, 4($sp)
	
	move $t4, $zero
	
	li $s1, 0x00 # blue max
	li $s2, 0xff # blue min
	
	li $s3, 0x00 # green max
	li $s4, 0xff # green min
	
	li $s5, 0x00 # red max
	li $s6, 0xff # red min
	
outer_loop2:
	move $t9, $t0
	add $t8, $t0, $t1
	
inner_loop2:
	lbu $t6, 0($t0)
	blt $t6, $s1, b_max_end
	move $s1, $t6
b_max_end:
	bgt $t6, $s2, b_min_end
	move $s2, $t6
b_min_end:
	
	lbu $t6, 1($t0)
	blt $t6, $s3, g_max_end
	move $s3, $t6
g_max_end:
	bgt $t6, $s4, g_min_end
	move $s4, $t6
g_min_end:

	lbu $t6, 2($t0)
	blt $t6, $s5, r_max_end
	move $s5, $t6
r_max_end:
	bgt $t6, $s6, r_min_end
	move $s6, $t6
r_min_end:

	addiu $t0, $t0, 3
	blt $t0, $t8, inner_loop2
	add $t0, $t9, $s0
	
	addiu $t4, $t4, 1
	blt $t4, $t3, outer_loop2

	li $t0, 0
	
	sub $s1, $s1, $s2
	sub $s3, $s3, $s4
	sub $s5, $s5, $s6
	
setting_up_LUT:
	
	sub $t1, $t0, $s2	# blue
	mul $t1, $t1, 0xff
	div $t1, $t1, $s1
	sb $t1, LUT($t0)
	
	sub $t1, $t0, $s4	# green
	mul $t1, $t1, 0xff
	div $t1, $t1, $s3
	sb $t1, LUT2($t0)
	
	sub $t1, $t0, $s6	# red
	mul $t1, $t1, 0xff
	div $t1, $t1, $s5
	sb $t1, LUT3($t0)
	
	addiu $t0, $t0, 1
	ble $t0, 0xff, setting_up_LUT
	
	lw $t1, bitmapFileHeader+12   # offset to pixel array
	lw $t2, bitmapFileHeader+0x14 # width
	lw $t3, bitmapFileHeader+0x18 # height
	
	la $t0, bitmapFileHeader+2
	add $t0, $t0, $t1
	lw $t1, 4($sp)
	lw $s0, 0($sp)
	move $t4, $zero
	
outer_loop3:
	move $t9, $t0
	add $t8, $t0, $t1
	
inner_loop3:
	
	lbu $t6, 0($t0)
	lbu $t6, LUT($t6)
	sb  $t6, 0($t0)
	
	lbu $t6, 1($t0)
	lbu $t6, LUT2($t6)
	sb  $t6, 1($t0)
	
	lbu $t6, 2($t0)
	lbu $t6, LUT3($t6)
	sb  $t6, 2($t0)
	
	addiu $t0, $t0, 3
	blt $t0, $t8, inner_loop3
	add $t0, $t9, $s0
	
	addiu $t4, $t4, 1
	blt $t4, $t3, outer_loop3
	addiu $sp, $sp, 8
	
	j write_result
	
