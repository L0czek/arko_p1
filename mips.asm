.data

.eqv SYS_READLINE 8
.eqv SYS_WRITELINE 4
.eqv SYS_OPEN 13
.eqv SYS_READ 14
.eqv SYS_WRITE 15
.eqv SYS_READINT 5
.eqv SYS_CLOSE 16
.eqv SYS_WRITE 15
.eqv SYS_PRINT_INT 1
.eqv SYS_EXIT 17

.eqv BUFFER_SIZE 0xa000000 
#.eqv BUFFER_SIZE 0x10000
#.eqv BUFFER_SIZE 0x100
np: .asciiz "Podaj nazwe pliku >"
error: .asciiz "Blad otwarcia pliku.\n"
menu: .asciiz "Wybierz operacje.\n\t1 => Zmiana kontrastu\n\t2 => Rozciaganie histogramu\n\t3 => Wyjscie\nopcja> "
ferror: .asciiz "To nie jest plik bmp 24 bitowy\n"
pkontrast: .asciiz "Podaj współczynnik zmiany kontrastu ( 100 to kontrast bazowy) \nkontrast> "
wynik: .asciiz "Podaj nazwe pliku wynikowego> "
error_w: .asciiz "Nie mozna otworzyć pliku do zapisu\n"

.align 2
chunks: .word 0
minValues: .space 4
maxValues: .space 4
LUT0: .space 0x300
.align 2
rowSizeInBytes: .word 0
rowPaddingSize: .word 0
chunkSize: .word 0
sizeOfHeaders: .word 0
path: .space 0x1000
bitmapFileHeader: .space 16
buffer: .space BUFFER_SIZE

.text

.macro cmov(%where, %what, %condition,%temp)
	xori %temp, %condition, 1
	mul %condition, %condition, %what
	mul %temp, %temp, %where
	or %where, %condition, %temp
.end_macro

start:
	li $v0, SYS_WRITELINE
	la $a0, menu
	syscall
	
	li $v0, SYS_READLINE
	la $a0, buffer
	li $a1, BUFFER_SIZE
	syscall
	
	la $a0, buffer
	jal stoi
	
	sge $t0, $v0, 4
	sle $t1, $v0, 0
	or $t0, $t0, $t1
	
	bnez $t0, start	
	move $s0, $v0 # s0 -> choice
	beq $v0, 3, pexit
readPath:
	li $v0, SYS_WRITELINE
	la $a0, np
	syscall
	
	li $v0, SYS_READLINE
	la $a0, path
	li $a1, 0x1000
	syscall
	
	la $t0, path
	jal path_loop1
	
openFile:
	li $v0, SYS_OPEN # open for reading
 	la $a0, path
	move $a1, $zero
	move $a2, $zero
	syscall
	
	bne $v0, -1, successfulOpen
	
	li $v0, SYS_WRITELINE
	la $a0, error
	syscall
	j readPath 
	
successfulOpen:
	move $s1, $v0 # file in descriptor
readBitmapHeader:
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, bitmapFileHeader+2
	li $a2, 14
	syscall

	lw $t0, bitmapFileHeader
	beq $t0, 0x4d420000, readOutputFileName
	
	li $v0, SYS_WRITELINE
	la $a0, ferror
	syscall
	
	j readPath
		
readOutputFileName:
	li $v0, SYS_WRITELINE
	la $a0, wynik
	syscall
	
	li $v0, SYS_READLINE
	la $a0, buffer
	li $a1, BUFFER_SIZE
	syscall
	
	la $t0, buffer
	jal path_loop1
	
	li $v0, SYS_OPEN # open for writing
	la $a0, buffer
	li $a1, 1
	move $a2, $zero
	syscall
	
	bne $v0, -1, readOptionalsHeaders
	
	li $v0, SYS_WRITELINE
	la $a0, error
	syscall
	j readOutputFileName
	
readOptionalsHeaders:
	move $s2, $v0
	
	lw $t0, bitmapFileHeader+12 # offset to pixel array
	move $s4, $t0
	subiu $t0, $t0, 14
	
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, bitmapFileHeader+16
	move $a2, $t0
	syscall
	
	lw $t0, bitmapFileHeader+0x14
	lw $t1 bitmapFileHeader+0x18
	move $t6, $t1
	# chunks
	# chunkSize
	# rowSizeInbytes
	# padding
	mul $t0, $t0, 3
	move $t5, $t0
	sw $t0, rowSizeInBytes
	move $t4, $t0
	li $t2, 4
	div $t0, $t2
	mfhi $t0
	sub $t0, $t2, $t0
	andi $t0, $t0, 3
	sw $t0, rowPaddingSize
	add $t0, $t0, $t4
	li $t1, BUFFER_SIZE
	div $t1, $t1, $t0
	mul $t1, $t1, $t0
	bne $t1, 0, write_chunk_size
	li $t1, BUFFER_SIZE
	div $t1, $t1, 3
	mul $t1, $t1, 3
write_chunk_size:
	sw $t1, chunkSize
	mul $t0, $t5, $t6
	add $t0, $t0, $t1
	subiu $t0, $t0, 1
	div $t0, $t0, $t1
	sw $t0, chunks
	
	
	li $v0, SYS_WRITE
	move $a0, $s2
	la $a1, bitmapFileHeader+2
	move $a2, $s4
	sw $s4, sizeOfHeaders
	syscall

	beq $s0, 2, histogram_start

contrast_start:
	li $v0, SYS_WRITELINE
	la $a0, pkontrast
	syscall
	
	li $v0, SYS_READLINE
	la $a0, buffer
	li $a1, BUFFER_SIZE
	syscall
	
	la $a0, buffer
	jal stoi
	
	blt $v0, 0, contrast_start
	
	li $t2, 0xff
	move $t1, $zero
		
make_contrast_lut_loop:
	
	sub $t3, $t1, 127
	mul $t3, $t3, $v0
	div $t3, $t3, 100
	add $t3, $t3, 127
	
	sge $t4, $t3, $t2   # >= 0xff
	sge $t5, $t3, $zero # >= 0x00
	
	mul $t6, $t4, $t2
	or $t3, $t6, $t3
	mul $t6, $t5, $t2
	and $t3, $t6, $t3
	sb $t3, LUT0($t1)
	
	addiu $t1, $t1, 1
	ble $t1, 0xff, make_contrast_lut_loop
rewrite_contrast:
	move $t0, $zero
	lw $t1, rowPaddingSize
	lw $t2, rowSizeInBytes
	move $s6, $t2
	lw $t8, chunks
	move $t7, $zero
read_chunk_c:
	sub $t2, $t2, $t0
	move $t0, $zero
	#move $t2, $s6
	lw $t9, chunkSize
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, buffer
	move $a2, $t9
	syscall
	move $t9, $v0
contrast_loop:
	lbu $t3, buffer($t0)
	lbu $t3, LUT0($t3)
	sb $t3, buffer($t0)
	
	addiu $t0, $t0, 1
	slt $s0, $t0, $t2
	slt $s7, $t0, $t9
	and $s0, $s0, $s7
	#blt $t0, $t2, contrast_loop
	beq $s0, 1, contrast_loop
	add $t0, $t0, $t1
	add $t2, $t2, $t1
	add $t2, $t2, $s6
	beq $s7, 1, contrast_loop
read_more_c:
	li $v0, SYS_WRITE
	move $a0, $s2
	la $a1, buffer
	move $a2, $t9
	syscall
	
	addiu $t7, $t7, 1
	ble $t7, $t8, read_chunk_c 
	
	j conversion_end

histogram_start:
	move $t0, $zero
	subiu $t0, $t0, 1
	sll $t0, $t0, 8
	sw $t0, minValues
	move $t0, $zero
	lw $t1, rowPaddingSize
	lw $t2, rowSizeInBytes
	move $s6, $t2
	lw $t8, chunks
	move $t7, $zero
read_chunk_h:
	move $t0, $zero
	move $t2, $s6
	lw $t9, chunkSize
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, buffer
	move $a2, $t9
	syscall
	move $t9, $v0
histogram_loop:
	
	li $t3, 3
h_inner_loop:
	
	lbu $t4, buffer($t0)
	
	lbu $t5, maxValues($t3)
	lbu $t6, minValues($t3)
	
	sgt $s4, $t4, $t5
	cmov($t5, $t4, $s4, $s5) 
	slt $s4, $t4, $t6
	cmov($t6, $t4, $s4, $s5)
		
	sb $t5, maxValues($t3) 
	sb $t6, minValues($t3)
	
	addiu $t0, $t0, 1
	subiu $t3, $t3, 1
	bnez $t3, h_inner_loop
	
	slt $s0, $t0, $t2
	slt $s7, $t0, $t9
	and $s0, $s0, $s7
	beq $s0, 1, histogram_loop
	add $t0, $t0, $t1
	add $t2, $t2, $t1
	add $t2, $s6, $t2
	beq $s7, 1, histogram_loop
	
	addiu $t7, $t7, 1
	blt $t7, $t8, read_chunk_h
	
	# make LUT
	lw $t0, maxValues
	lw $t1, minValues
	sub $t0, $t0, $t1
	sw $t0, maxValues
	move $t0, $zero
	la $t5, LUT0
make_histogram_lut:
	li $t1, 3
	add $t6, $t0, $t5
lut_inner_loop:	
	lbu $t2, maxValues($t1)
	lbu $t3, minValues($t1)
	sub $t4, $t0, $t3
	mul $t4, $t4, 0xff
	div $t4, $t4, $t2
	sb $t4, 0($t6)
	
	addiu $t6, $t6, 256
	subiu $t1, $t1, 1
	bgt $t1, 0, lut_inner_loop
	
	addiu $t0, $t0, 1
	ble $t0, 0xff, make_histogram_lut

	la $t5, LUT0	
	move $t6, $zero
	move $t0, $zero
	lw $t1, rowPaddingSize
	lw $t2, rowSizeInBytes
	lw $t8, chunks
	move $t7, $zero
		
	lw $t0, chunks
	beq $t0, 1, after_read
	 
	
	li $v0, SYS_CLOSE
	move $a0, $s1
	syscall
	
	li $v0, SYS_OPEN # open for reading
	la $a0, path
	move $a1, $zero
	move $a2, $zero	 
	syscall
	
	move $s1, $v0
	
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, buffer
	lw $a2, sizeOfHeaders
	syscall

read_chunk_rewrite:

	lw $t9, chunkSize
	li $v0, SYS_READ
	move $a0, $s1
	la $a1, buffer
	move $a2, $t9
	syscall
after_read:
	move $t0, $zero
	move $t2, $s6
	move $t9, $v0
histogram_loop_rewrite:
	move $t3, $zero
h_inner_loop_rewrite:
	
	lbu $t4, buffer($t0)
	#mul $t6, $t3, 256
	sll $t6, $t3, 8
	add $t6, $t6, $t4
	lbu $t6, LUT0($t6)
	sb $t6, buffer($t0)
	
	addiu $t0, $t0, 1
	addiu $t3, $t3, 1
	blt $t3, 3, h_inner_loop_rewrite
	
	slt $s0, $t0, $t2
	slt $s7, $t0, $t9
	and $s0, $s0, $s7
	beq $s0, 1, histogram_loop_rewrite
	add $t0, $t0, $t1
	add $t2, $t2, $t1
	add $t2, $t2, $s6
	beq $s7, 1, histogram_loop_rewrite
	
	li $v0, SYS_WRITE
	move $a0, $s2
	la $a1, buffer
	move $a2, $t9
	syscall
	
	addiu $t7, $t7, 1
	blt $t7, $t8, read_chunk_rewrite	
	
	j conversion_end
	
path_loop1:
	lbu $t1, 0($t0)
	addiu $t0, $t0, 1
	bne $t1, '\n', path_loop1
	sb $zero, -1($t0)
	jr $ra
	
stoi_error:
	li $v0, -1
	jr $ra
		
stoi: # int stoi(char * $a0)
	move $v0, $zero
	lbu $t3, 0($a0)
stoi_inner_loop:
	move $t0, $t3
	lbu $t3, 1($a0)
	sge $t1, $t0, 0x3a
	sle $t2, $t0, 0x2f

	or $t1, $t1, $t2
	bnez $t1, stoi_error
	mul $v0, $v0, 10
	subiu $t0, $t0, '0'
	add $v0, $v0, $t0
	addiu $a0, $a0, 1
	bne $t3, '\n', stoi_inner_loop
	nop
	jr $ra
	
conversion_end:
	li $v0, SYS_CLOSE
	move $a0, $s1
	syscall
	
	li $v0, SYS_CLOSE	
	move $a0, $s2
	syscall
	j start
pexit:
	li $v0, SYS_EXIT
	move $a0, $zero
	syscall
